#coding=gbk
# IDE: PyCharm 3.4.1, Version: Python 2.7.8
# Created in July 2014 by Xiao Shikang

import os
import time
import socket

from scapy.all import *
conf.verb = 0

print "TCP旁路劫持检测器(TCP Bypass-Hijacking Detector) for Windows v0.6"
print "注意：使用过程中请不要切换网卡或断网重新连接或使用迅雷,否则可能产生误报"
print "遇劫持请到工信部投诉：http:www.chinatcc.gov.cn:8080/cms/shensus/"

# 路由节点跳数差容忍度
ttl_diff_tol = 4

# 日志文件
fn = time.strftime("%Y-%m-%d", time.localtime()) + ".log"
fo = open(fn, 'a')


def p(inf):  # 在终端打印警告信息并写日志
    print inf
    fo.write(time.strftime("[%m-%d %H:%M] ", time.localtime()) + inf + "\r\n")


print "侦听中...您现在可以进行上网工作，程序将自动记录日志"
print "日志文件：" + os.path.dirname(os.path.realpath(__file__)) + "\\" + fn

cache_ttl_sa = {}   # [SYN,ACK]握手包TTL缓存
cache_ttl_a = {}    # [ACK]普通包TTL缓存

while 1:
    # 网络连接侦听过滤设定
    myIp = socket.gethostbyname(socket.gethostname())
    con = sniff(filter="tcp and dst host "+myIp, count=66)

    for item in con:
        # 获取TTL值，若TTL合法继续获取其它信息
        ttl = item.sprintf(r"%IP.ttl%")
        if ttl.isdigit():
            ip = item.sprintf(r"%IP.src%")         # 获取IP地址
            flags = item.sprintf(r"%TCP.flags%")   # 获取标志位
            if ip == '??' or flags == '??':
                continue

            if flags == 'SA':  # 两次通信握手包TTL对比检查
                if ip in cache_ttl_sa and abs(int(ttl)-int(cache_ttl_sa[ip])) > ttl_diff_tol:
                    p("[警告]与" + ip + "通信过程中握手TTL差过大(旧:" + cache_ttl_sa[ip] + ",新:" + ttl + ")可能被劫持")
                else:
                    cache_ttl_sa[ip] = ttl

            elif flags == 'FPA' and item.sprintf(r"%Raw.load%") != '??':  # 运营商强行跳转特征
                http_header = item.sprintf(r"%Raw.load%")
                hh = http_header.split(r"\r\n\r\n")[0].split(r"\r\n")
                status = hh[0].split(" ")
                if len(status) > 1 and "HTTP" in status[0] and "30" in status[1]:  # HTTP 30[1-3]
                    for hhi in hh:
                        if r"Location: " in hhi and r".js" in hhi:
                            p("[警告]与" + ip + "通信过程中发现伪造脚本劫持，" + hhi)
                            break
                else:
                    p("[警告]与" + ip + "通信过程中发现了标志位异常的数据包")

            elif 'R' in flags:  # 网络被重置特征
                if ip in cache_ttl_a and abs(int(ttl)-int(cache_ttl_a[ip])) > ttl_diff_tol:
                    p("[警告]与" + ip + "通信过程中连接可能被劫持者重置(旧:" + cache_ttl_a[ip] + ",新:" + ttl + ")")

            elif 'F' in flags:  # FIN意外数据包，传输内容可能被劫持者置换并随即切断连接
                if ip in cache_ttl_a and abs(int(ttl)-int(cache_ttl_a[ip])) > ttl_diff_tol:
                    http_header = item.sprintf(r"%Raw.load%")
                    if http_header == '??':
                        p("[警告]与" + ip + "通信过程中TTL异常变化(旧:" + cache_ttl_a[ip] + ",新:" + ttl + ")")
                    else:
                        hh = http_header.split(r"\r\n\r\n")[0]
                        status = hh.split(r"\r\n")[0].split(" ")
                        if len(status) > 1 and "HTTP" in status[0] and "200" == status[1] and len(hh) > 1:  # HTTP 200
                            p("[警告]与" + ip + "通信过程中数据可能被劫持置换：" + hh[1])

            elif flags == 'A':
                if ip in cache_ttl_a and abs(int(ttl)-int(cache_ttl_a[ip])) > ttl_diff_tol:
                    p("[警告]与" + ip + "通信过程中TTL异常变化(旧:" + cache_ttl_a[ip] + ",新:" + ttl + ")")
                else:
                    cache_ttl_a[ip] = ttl

fo.close()
